﻿var express = require('express'),
    soap = require('node-soap'),
    httpserver = require('http'),
    path = require('path');

var app = express();

function WebService(server, port) {

    this.server = server;
    this.port = port;

    var ws = httpserver.createServer(app);

    // TODO EntryPoint SOAP
    // soap.listen(server, '/soap', ircwsdl , xml);

    ws.listen(this.port, function () {
        var host = ws.address().address;
        var port = ws.address().port;

        console.log("Web Service escutando em http://%s:%s", host, port);
        console.log("Requisições REST no context /rest");
        // console.log("Requisições REST no context /soap");
    });
}

WebService.initialize = function (server, port) {
    var webservice = new WebService(server, port);
    webservice.init();
};

WebService.prototype = {

    init: function initializeEntryPoints() {

        var webservice = this;

        app.all('/*', function (req, res, next) {
            res.header("Access-Control-Allow-Origin", "*");
            res.header("Access-Control-Allow-Headers", "X-Requested-With");
            next();
        });

        app.get(/\/client\//, function (req, res) {

            var url = req.originalUrl.match(/\/client\/(.*)/);

            if ((!url) || (!url[1])) {
                res.sendFile(path.join(__dirname, '../../irc-web/', 'index.html'));
            }
            else {
                var dir = url[1].match(/^(.*)\/(.*)$/)[1];
                var file = url[1].match(/^(.*)\/(.*)$/)[2];
                res.sendFile(path.join(__dirname, '../../irc-web/' + dir, file));
            }
        });

        app.get(/\/rest\//, function (req, res) {

            var url = req.originalUrl.match(/\/rest\/(.*)/)[1].split('/');
            var pass = url[0];
            var command = (url.length > 1) ? url[1].toUpperCase() : false;
            var args = (url.length > 2) ? url.slice(2, url.length) : false;
            var address = req.headers['x-forwarded-for'] || req.connection.remoteAddress; //Pega o IP (parece não funcionar em redes locais)

            for (var i = 0; i < args.length; i++) {
                args[i] = decodeURIComponent(args[i]);
            }

            //Recupera usuário pelo IP e Pass (hash)
            var user = webservice.server.getUser(address, pass);

            //Se existir o usuário
            if (user) {

                //Renova o timeout da conexão
                user.countdown();
                //Seta response no objeto do Usuário
                user.set('socket', res);

                //Se não há comando, apenas retorna mensagens armazenadas
                if (!command)
                    user.write();

                //É necessário se registrar antes de executar comandos
                else if (!user.isRegistered()) {
                    if ((command == 'NICK') || (command == 'USER')) {
                        webservice.server.send(user, {command: command, args: args});
                    }
                    else {
                        user.reply(451); //ERR_NOTREGISTERED
                    }
                }

                //Executa comando
                else {
                    webservice.server.send(user, {command: command, args: args});
                }
            }
            //Usuário não existe
            else if (command == "PASS") {
                user = webservice.server.setWebUser(res, address, pass);
                user.response("self", "PASS").send();
            }
            //Usuário não existe e conexão não foi registrada
            else {
                res.end(JSON.stringify({error: 'Invalid request'}));
            }
        });
    }
};

exports.WebService = WebService;