var fs = require('fs'), 
    Channel = require('./channel'), 
    User   = require('./user');

function Commands(server, args) {
    this.server = server;
    this.args = args;
}

Commands.prototype = {

    TEST: function(user) { 
        this.server.debug("Commands.TEST()");
        //Operação para testes
    }, 
    
    HELP: function(user) { 
        this.server.debug("Commands.HELP()");
        
        //RPL_HELP
        user.reply(705, {string: "/NICK Configurar um nickname ou alterar existente | /NICK <nickname>"});
        user.reply(705, {string: "/USER Configurar o username, hostname e realname  | /USER <username> <hostname> <realname>"});
        user.reply(705, {string: "/JOIN Entrar em determinado canal | /JOIN <canal>"});
        user.reply(705, {string: "/TOPIC Configurar e visualizar tópico do canal | /TOPIC <canal> tópico ou /TOPIC <canal>"});
        user.reply(705, {string: "/LIST Lista os canais e seus tópicos"});
        user.reply(705, {string: "/PART Sair do canal | /PART <canal>"});
        user.reply(705, {string: "/VERSION Versão atual"});
        user.reply(705, {string: "/TIME Ver Data e Horas"});
        user.reply(705, {string: "/INFO Informacoes gerais"});
        user.reply(705, {string: "/BAN Bane um usuário | /BAN <canal> <nickname>"});
        user.reply(705, {string: "/QUIT Sair da sessão | /QUIT <mensagem>"});
        user.reply(706); //RPL_ENDOFHELP
    }, 

    MSG: function(user, args) {

        return this.PRIVMSG(user, args);
    }, 

    PRIVMSG: function(user, args) {
        
        this.server.debug("Commands.PRIVMSG()");

        //Destinatário da mensagem
        var to  = args[0];
        var msg = args[1];
        
        //Se houver conteúdo na mensagem
        if (msg) {

            //Se for para um canal
            if ((to[0].match(/[#!+&]/))&&(to.length > 1)) {
                
                var channel = this.server.getChannel(to);
                var nick = user.nickname;

                //Se existir, envia para todos usuários do canal
                if (channel) {

                    //Usuário não está no canal
                    if (!channel.getUser(user.id)) {
                        user.reply(442, {channel: to}); //ERR_NOTONCHANNEL
                        return false;
                    }
                    else {
                        channel.write(user.response('self', 'PRIVMSG', msg, channel.name).message);
                        return true;
                    }
                }
                //Erro: Canal não existe
                else{
                    user.reply(404, {'channel name': to}); //ERR_CANNOTSENDTOCHAN
                    return false;
                }
            }
            //Se for para um usuário
            else if ((to.match(/[\w\-_]+/))&&(to.length <= 9)) {
                
                var users  = this.server.get('users');
                var toUser = false;

                for (var i=0; i<users.length; i++) {
                    if (users[i]) {
                        if (users[i].get('nickname') === to) {
                            toUser = users[i];
                            break;
                        }
                    }
                }
                //Se o usuário existir, envia mensagem privada
                if (toUser) {
                    console.log(user.response('self', 'PRIVMSG', msg, toUser.nickname).message);
                    toUser.write(user.response('self', 'PRIVMSG', msg, toUser.nickname).message);

                    //Se o usuário estiver AWAY, retorna mensagem away
                    if (toUser.getMode('a')) {
                        user.reply(301, {nick: toUser.nickname, 'away message': toUser.awayMsg});
                    }

                    return true;
                }
                //Se o usuário não existir, retorna erro
                else{
                    user.reply(401, {nickname: to}); //ERR_NOSUCHNICK
                    return false;
                }
            }
            //Usuário não enviou mensagem para nenhum destinatário
            else {
                user.reply(411, {command: "PRIVMSG"}); //ERR_NORECIPIENT
                return false;
            }
        }
        else{
            user.reply(412); //ERR_NOTEXTTOSEND
            return false;
        }
    }, 

    NOTICE: function(user, args) {
        
        this.server.debug("Commands.NOTICE()");

        //Destinatário da mensagem
        var to  = args[0];
        var msg = args[1];
        
        //Se houver conteúdo na mensagem
        if (msg) {

            //Se for para um canal
            if ((to[0].match(/[#!+&]/))&&(to.length > 1)) {
                
                var channel = this.server.getChannel(to);
                var nick = user.nickname;

                //Se existir, envia para todos usuários do canal
                if (channel) {

                    //Usuário não está no canal
                    if (!channel.getUser(user.id)) {
                        //user.reply(442, {channel: to}); //ERR_NOTONCHANNEL
                        return false;
                    }
                    else {
                        channel.write(user.response('self', 'NOTICE', msg, channel.name).message);
                        return true;
                    }
                }
                //Erro: Canal não existe
                else{
                    //user.reply(404, {'channel name': to}); //ERR_CANNOTSENDTOCHAN
                    return false;
                }
            }
            //Se for para um usuário
            else if ((to.match(/[\w\-_]+/))&&(to.length <= 9)) {
                
                var users  = this.server.get('users');
                var toUser = false;

                for (var i=0; i<users.length; i++) {
                    if (users[i]) {
                        if (users[i].get('nickname') === to) {
                            toUser = users[i];
                            break;
                        }
                    }
                }
                //Se o usuário existir, envia mensagem privada
                if (toUser) {
                    console.log(user.response('self', 'NOTICE', msg, toUser.nickname).message);
                    toUser.write(user.response('self', 'NOTICE', msg, toUser.nickname).message);

                    //Se o usuário estiver AWAY, retorna mensagem away
                    if (toUser.getMode('a')) {
                        user.reply(301, {nick: toUser.nickname, 'away message': toUser.awayMsg});
                    }

                    return true;
                }
                //Se o usuário não existir, retorna erro
                else{
                    //user.reply(401, {nickname: to}); //ERR_NOSUCHNICK
                    return false;
                }
            }
            //Usuário não enviou mensagem para nenhum destinatário
            else {
                //user.reply(411, {command: "PRIVMSG"}); //ERR_NORECIPIENT
                return false;
            }
        }
        else{
            //user.reply(412); //ERR_NOTEXTTOSEND
            return false;
        }
    }, 

    AWAY: function(user, args) {    
        
        if(args[0]){
            user.setMode(true, "a");
            user.set("awayMsg", args[0]);
            user.reply(306);
        }
        else{
            user.setMode(false, "a");
            user.set("awayMsg", "");
            user.reply(305);
        }
    }, 

    PING: function (user, args) {
        
        this.server.debug("Commands.PING()");
        
        user.response('server', 'PONG', args[0], 'self').send();

        return true;
    }, 

    NICK: function(user, args) {    
        
        this.server.debug("Commands.NICK()");

        //Faltam parâmetros
        if(!args[0]){
            user.reply(431); //ERR_NONICKNAMEGIVEN
            return false;
        }

        //Remove espaços e parágrafos
        var nick  = args[0].replace(/[\s]/g, '');

        //Se o nickname for igual o atual nick do usuário, não faz nada
        if(user.nickname === nick) return false;

        //Tamanho máximo de 9 caracteres (se for maior, corta o nick)
        if(nick.length > 9){ nick = nick.slice(0,9); }
        
        //Checa se algum usuário está usando o nickname
        for(var i=0; i<this.server.users.length; i++){
            
            anUser = this.server.users[i];

            //Erro: nickname já está sendo utilizado
            if((anUser != null)&&(anUser.nickname.toUpperCase() === nick.toUpperCase())){
                user.reply(433, {nick: nick}); //ERR_NICKNAMEINUSE
                return false;
            }
        }
        
        //Erro: formato incorreto
        if( (!nick.match(/^([\w_-]+)$/)) || (nick.match(/^guest_([\d]+)$/)) || (nick.match(/^([\d])(.*)/)) ){
            user.reply(432, {nick: nick}); //ERR_ERRONEOUSNICKNAME
            return false;
        }

        //Anuncia mudança de nome do usuário nos canais que ele está
        user.response("self", "NICK", nick).broadcast();

        //Muda o nickname do usuário
        user.nickname = nick;
        return true;
    }, 

    JOIN: function(user, args) {  
        
        this.server.debug("Commands.JOIN()");
        
        //Argumentos insuficientes
        if (!args[0]) {
            user.reply(461, {command: "JOIN"}); //ERR_NEEDMOREPARAMS
            return false;
        }
        
        var channel = args[0];

        //Extrai nome do canal (converte canais sem prefixo em #)
        if (!channel[0].match(/[!#&+]/)) {
            channel = "#"+channel;
        }

        //Safe Channel - nome incorreto
        if ((channel[0] == '!')&&(! channel.match(/^!([\w]+)$/))) {
            user.reply(437, {name: channel});
            return false;
        }

        //Se o canal já existir adiciona o usuário como membro
        if (this.server.getChannel(channel)) {
            //Se o usuário ainda não for um membro, adiciona
            if (!this.server.getChannel(channel).getUser(user.id)) {
                this.server.getChannel(channel).addUser(user);
            }
            else{
                return false;
            }
        }
        else { //Se o canal não existir, cria um novo e atribui o usuário como criador
            this.server.addChannel(channel, user);
        }

        //Anuncia novo usuário para o canal
        this.server.getChannel(channel).write(user.response('self', 'JOIN', '', channel).message);
        
        this.TOPIC(user, [channel]); //Envia o tópico atual do canal
        this.NAMES(user, [channel]); //Envia membros do canal

        return true;
    }, 

    PART: function(user, args) {    
        
        if(!args[0]){
            user.reply(461, {command: "PART"}); //ERR_NEEDMOREPARAMS
            return false;
        }

        var channel = this.server.getChannel(args[0]);

        if(!channel){
            user.reply(403, {'channel name': args[0]}); //ERR_NOSUCHCHANNEL
            return false;
        }

        if(!channel.getUser(user.id)){ 
            user.reply(442, {channel: channel.name}); //ERR_NOTONCHANNEL
            return false;
        }

        var msg = args[1];
        
        //Anuncia a saída e remove o usuário
        channel.write(user.response('self', 'PART', msg, channel.name).message);
        channel.removeUser(user);
        
        return true;
    }, 

    TOPIC: function(user, args) {  

        this.server.debug("Commands.TOPIC()");

        if (!args[0]) {
            user.reply(461, {command: "TOPIC"}); //ERR_NEEDMOREPARAMS
            return false;
        }

        var channel = this.server.getChannel(args[0]);

        if (!channel) {
            user.reply(403, {'channel name': args[0]}); //ERR_NOSUCHCHANNEL
            return false;
        }

        if (!channel.getUser(user.id)) {
            user.reply(442, {channel: channel.name}); //ERR_NOTONCHANNEL
            return false;
        }

        //Retorna o tópico
        if (!args[1]) {
            var topic = channel.get('topic');

            if (!topic) {
                user.reply(331, {channel: channel.name}); //RPL_NOTOPIC
                return false;
            }
            else {
                user.reply(332, {channel: channel.name, topic: topic}); //RPL_TOPIC
                return true;
            }
        }

        //Seta o tópico
        else {
            var topic = args[1];

            if (!channel.isOper(user.id)) {
                user.reply(482, {channel: channel.name}); //ERR_CHANOPRIVSNEEDED
                return false;
            }
            else if(channel.get('topic') != topic){
                channel.set('topic', topic);
                user.reply(332, {channel: channel.name, topic: topic}); //RPL_TOPIC
                return true;
            }
            else {
                return false;
            }
        }
    }, 

    PASS: function(user, args) {
        
        this.server.debug("Commands.PASS()");
        
        if (!args[0]) {
            user.reply(461, {command: "PASS"}); //ERR_NEEDMOREPARAMS
            return false;
        }
        
        if (user.isRegistered()) {
            user.reply(462); //ERR_ALREADYREGISTERED
            return false;
        }

        else{
            user.set('password', args[0]);
            return true;
        }
    }, 

    USER: function(user, args) {
        
        this.server.debug("Commands.USER()");
        
        //Usuário já está registrado
        if (user.isRegistered()) {
            user.reply(462); //ERR_ALREADYREGISTERED
            return false;
        }

        //Parâmetros insuficientes (user, mode, unused, realname)
        if ((args.length < 4)||(!args[0])||(!args[3])) {
            user.reply(461, {command: "USER"}); //ERR_NEEDMOREPARAMS
            return false;
        }

        user.username = args[0];
        user.realname = args[3];
        
        //Mensagem de registro
        user.reply(001, { 'nick': user.nickname, 'user': user.username, 'host': user.host }); //RPL_WELCOME
        return true;
    }, 

    OPER: function(user, args) {
        
        this.server.debug("Commands.OPER()");

        //Se já for um Operador
        if (user.getMode('o')) {
            return false;
        }

        if ((!args[0] )||(!args[1])) {
            user.reply(461, {command: "OPER"}); //ERR_NEEDMOREPARAMS
            return false;
        }

        var opers = this.server.get('opers');
        var login = false;

        opers.forEach(function(oper) {
            if ((oper.user === args[0]) && (oper.pass === args[1])) login = true;
        });

        if (login) {
            user.setMode(true, "o");
            this.server.opersOnline++;
            user.reply(381); //RPL_YOUREOPER
            return true;
        }
        else{
            user.reply(464); //ERR_PASSWDMISMATCH
            return false;
        }
    }, 

    MODE: function(user, args) { 
        
        this.server.debug("Commands.MODE()");

        var target = user;

        //Não enviou parâmetros suficientes
        if ((args[1])&&((args[1].length < 2)||(!args[1][0].match(/[\-+]/)))) {
            user.reply(461, {command: "MODE"}); //ERR_NEEDMOREPARAMS
            return false;
        }

        //Se o alvo for um outro usuário
        if ((args[0])&&(user.get('nickname') != args[0])) {

            //Usuário comum não pode mudar o modo de outro usuário
            if (!user.getMode('o')) {
                user.reply(502); //ERR_USERSDONTMATCH
                return false;
            }
            else {
                
                target = this.server.getUser(args[0]);

                //Usuário não existe
                if (target == false) {
                    user.reply(401, {nickname: to}); //ERR_NOSUCHNICK
                    return false;
                }
                
                //Se o usuário também for um operador do IRC
                else if (target.getMode('o')) {
                    user.reply(502); //ERR_USERSDONTMATCH
                    return false;
                }
            }
        }

        //Retorna todos os modos do usuário
        if (!args[1]) {
            //Há duas formas de se responder, com Command ou Reply
            user.reply(221, {'user mode string': target.getModes()}); //RPL_UMODEIS
            // user.response('self', 'MODE', target.getModes(), 'self');
            return true;
        }

        //Prepara os dados
        var type  = args[1][0];
        var flags = args[1].toLowerCase();
        
        //Seta os modos
        for(var i=1; i<flags.length; i++) {

            //Proibido mudar 'a'. Proibido ganhar 'o'. Proibido droppar 'r'
            if (!(type+flags[i]).match(/\+o|\-r|\+a|\-a/)) {
                
                //Realiza mudança
                if (!target.setMode(type, flags[i])) {

                    //Modo desconhecido
                    user.reply(501); //ERR_UMODEUNKNOWNFLAG
                    return false;
                }

                if(flags[i] == 'o') this.server.opersOnline--;
            }
        }

        return true;
    }, 

    QUIT: function (user, args) {  
        
        this.server.debug("Commands.QUIT()");
        
        var msg = (args[0])? args[0] : "";
        
        for(var i=0; i<user.channels.length; i++) {
            var channel = this.server.getChannel(user.channels[i]);
            var name = channel.name;
            channel.removeUser(user);
            if(this.server.getChannel(name)) channel.write(user.response('self', 'QUIT', msg, channel.name).message);
        }
        
        if(user.getMode('o')){
            this.server.opersOnline--;
        }

        delete this.server.users[user.id];

        this.server.connectionsCount--;
        
        user.destroy();
    }, 

    SQUIT: function(user, args) {  
        
        this.server.debug("Commands.SQUIT()");

        if(!user.getMode('o')){
            user.reply(481); //ERR_NOPRIVILEGES
            return false;
        }

        if((!args[1])||(args.length < 2)){
            user.reply(461, {command: "SQUIT"}); //ERR_NEEDMOREPARAMS
            return false;
        }

        if(args[0] != this.server.get('address')){
            user.reply(402, {'server name': args[0]}); //ERR_NOSUCHSERVER
            return false;
        }

        this.server.destroy(args[1]);
        return true;
    }, 

    INFO: function (user) {

        this.server.debug("Commands.INFO()");

        //RPL_INFO
        user.reply(371, {string: "Bem-vindo(a) ao servidor IRC do Curso de Sistemas de Informação"});
        user.reply(371, {string: "Disciplina de Aplicações Distribuidas da Universidade Federal de Goiás"});
        user.reply(371, {string: "GRUPO 1: Operações de Canal"});
        user.reply(371, {string: "GRUPO 2: Registro de Conexão"});
        user.reply(371, {string: "GRUPO 3: Consultas Baseadas no Usuário"});
        user.reply(371, {string: "GRUPO 4: Consultas ao Servidor"});
        user.reply(371, {string: "GRUPO 5: Mensagens Privadas"});
        user.reply(371, {string: "Informações sobre o servidor:"});
        user.reply(371, {string: "Versão: " + this.server.get('version')});
        user.reply(371, {string: "Número de usuários conectados: "+ this.server.connectionsCount});
        user.reply(371, {string: "Tempo de vida do servidor: " + process.hrtime()});
        user.reply(371, {string: "Administrador do Servidor: Marcelo Akira Inuzuka"});
        user.reply(374); //RPL_ENDOFINFO
        
        return true;
    }, 

    ADMIN: function (user, args) {  

        if((args[0])&&(args[0] != this.server.get('address'))){
            user.rpl(402, {'server name': args[0]}); //ERR_NOSUCHSERVER
            return false;
        }

        user.reply(256, {server: this.server.get('address')}); //RPL_ADMINME
        user.reply(257, {'admin info': "Administrador do servidor: "+this.server.get('admin').name}); //RPL_ADMINLOC1
        user.reply(258, {'admin info': this.server.get('admin').info}); //RPL_ADMINLOC2
        user.reply(259, {'admin info': "E-mail: "+this.server.get('admin').email}); //RPL_ADMINEMAIL
        
        return true;
    }, 
    
    VERSION: function (user) {

        this.server.debug('Commands.VERSION()');

        user.reply(351, {
            version: this.server.get('version'), 
            debuglevel: (this.server.get('debugMode'))? '1' : '0', 
            server: this.server.get('address'), comments: ''
        }); //RPL_VERSION

        return true;
    }, 

    TIME: function (user) {
        
        var date = new Date();
        var time = date.getUTCDay()+"/"+date.getUTCMonth()+"/"+date.getUTCFullYear()+" "+date.getUTCHours()+":"+date.getUTCMinutes();

        user.reply(391, {server: this.server.get('name'), time: time});
        
        return true;
    }, 

    LUSERS: function (user, args) {  

        if((args[0])&&(args[0] != this.server.get('address'))){
            user.rpl(402, {'server name': args[0]}); //ERR_NOSUCHSERVER
            return false;
        }

        user.reply(251, {users: this.server.get('connectionsCount'), services: 0, servers: 1}); //RPL_LUSERCLIENT
        user.reply(252, {integer: this.server.get('opersOnline')}); //RPL_LUSEROP
        user.reply(253, {integer: 0}); //RPL_LUSERUNKOWN
        user.reply(254, {integer: this.server.get('channels').length}); //RPL_LUSERCHANNELS
        user.reply(255, {clients: this.server.get('connectionsCount'), servers: 1}); //RPL_LUSERME

        return true;
    }, 

    STATS: function (user, args) { 

        this.server.debug("Commands.STATS()");

        if (!args[0]) {
            //Parâmetros insuficientes
            user.reply(461, {command: "STATS"});
            return false;
        }

        var type = args[0].toUpperCase();

        //Converte o formato -L para L
        if(type[0] == "-") type = type[1];

        //Número de conexões do servidor
        if (type === "L") {
            user.reply(211, {
                linkname: this.server.get('name'), 
                'sendq'   :  '0', 
                'sent messages' :  this.server.get('sentCount'), 
                'sent Kbytes'     :  '?', 
                'received messages': this.server.get('receivedCount'), 
                'received Kbytes'  :  '?', 
                'time open'  :  process.hrtime()[0]
            }); //RPL_STATSLINKINFO
        }
        //Contagem de utilização dos comandos
        else if (type === "M") {
            
            commands = this.server.commandCount;
            
            Object.keys(this.server.commandCount).forEach(function (key) {
                user.reply(212, {command: key.toUpperCase(), count: commands[key], 'byte count': 0, 'remote count': 0});
            });
        }
        //Operadores do servidor
        else if (type === "O") {

            if (user.getMode("o")) {
                
                var opers = this.server.get('opers');
                
                opers.forEach(function(oper) {
                    user.reply(243, {hostmask:'*', name: oper.user});
                });
            }
            else{
                //Não é operador do IRC
                user.reply(481);
                return false;
            }
        }
        //Tempo de vida do servidor
        else if (type === "U") {
            user.reply(242, {time: process.hrtime()[0]+' milliseconds'});
        }
        else{
            user.reply(219, {'stats letter': type});
            return false;
        }

        user.reply(219, {'stats letter': type});
        return true;
    }, 
    
    MOTD: function (user) {  
        
        this.server.debug('Commands.MOTD()');

        var texto = fs.readFileSync('../files/motd', {encoding: 'utf8'}, function (erro, dados) {
            if (erro) user.reply(424, {'file op': 'MOTD', file: '../files/motd'}); //ERR_FILEERROR
        });

        user.reply(375, {server: this.server.get('name')}); //RPL_MOTDSTART

        var lines = texto.split(/\n/g);

        if((lines.length == 0)||(!lines[0])){
            user.reply(422); //ERR_NOMOTD
            return false;
        }



        for(var i=0; i<lines.length; i++){
            if(lines[i])
                user.reply(372, {text: lines[i]}); //RPL_MOTD
        }

        user.reply(376); //RPL_ENDOFMOTD
        return true;
    }, 

    LIST: function(user, args) {

        this.server.debug("Commands.LIST()");

        if (args[0]) {

            var channel = this.server.getChannel(args[0]);

            if (!channel) {
                user.reply(403, {'channel name': args[0]}); //ERR_NOSUCHCHANNEL
                return false;
            }

            user.reply(322, {
                channel: channel.name, 
                visible: channel.list().count, 
                topic: channel.get('topic') 
            }); //RPL_LIST
        }
        else {
            var channels = this.server.get('channels');

            for (var i=0; i<channels.length; i++) {
                
                channel = channels[i];

                user.reply(322, {
                    channel: channel.name, 
                    visible: channel.list().count, 
                    topic: channel.get('topic') 
                }); //RPL_LIST
            }
        }

        user.reply(323); //RPL_LISTEND
        return true;
    }, 

    NAMES: function(user, args) {

        this.server.debug('Commands.NAMES()');

        if (args[0]) {
            var channel = this.server.getChannel(args[0]);
            
            if (!channel) {
                user.reply(403, {'channel name': args[0]}); //ERR_NOSUCHCHANNEL
                return false;
            }

            user.reply(353, {type: "=", channel: channel.name, nicks: channel.list().text}); //RPL_NAMREPLY
        }
        else {

            var channels = this.server.get('channels');

            channels.forEach(function(channel) {
                user.reply(353, {type: "=", channel: channel.name, nicks: channel.list().text}); //RPL_NAMREPLY
            });

            user.reply(366, {channel: '*'}); //RPL_ENDOFNAMES
        }

        return true;
    }, 

    WHOIS: function(user, args) {

        this.server.debug('Commands.WHOIS()');        

        if(!args[0]){
            user.reply(431); //ERR_NONICKNAMEGIVEN
            return false;
        }
        
        if (args[1]) {
            var server = args[0];
            var nick   = args[1];

            if (server != this.server.get('address')) {
                user.reply(402, {'server name': server}); //ERR_NOSUCHSERVER
                return false;
            }
        }
        else {
            var nick   = args[0];
        }

        var whois = this.server.getUser(nick);

        if (!whois) {
            user.reply(401, {nickname: to}); //ERR_NOSUCHNICK
            return false;
        }

        // 311 RPL_WHOISUSER
        // "<nick> <user> <host> * :<real name>"
        user.reply(311, {nick: whois.get('nickname'), user: whois.get('username'), host: whois.get('host'), 'real name': whois.get('realname')});

        // 319 RPL_WHOISCHANNELS
        // "<nick> :(@|+) <channel>"
        var channels = whois.get('channels');
        for (var i=0; i<channels.length; i++) {
            var op = (this.server.getChannel(channels[i]).isOper(whois.get('id')))? '@' : '';
            user.reply(319, {nick: whois.get('nickname'), channel: op+channels[i]});
        }


        // 312 RPL_WHOISSERVER
        // "<nick> <server> :<server info>"
        user.reply(312, {nick: whois.get('nickname'), server: this.server.get('address'), 'server info': this.server.get('version')});

        // 301 RPL_AWAY
        // "<nick> :<away message>"
        if(whois.getMode('a')){
            user.reply(301, {nick: whois.get('nickname'), 'away message': whois.get('awayMsg')});
        }

        // 313 RPL_WHOISOPERATOR
        // "<nick> :is an IRC operator"
        if (whois.getMode('o')){
            user.reply(313, {nick: whois.get('nickname')});
        }

        // 317 RPL_WHOISIDLE
        // "<nick> <integer> :seconds idle"
        user.reply(317, {nick: whois.get('nickname'), integer: (new Date().getTime() - whois.get('access'))/1000});

        // 318 RPL_ENDOFWHOIS
        // "<nick> :End of WHOIS list"
        user.reply(318, {nick: whois.get('nickname')});

        return true;
    }, 

    BAN: function(user, args) {

        this.server.debug('Commands.BAN()');

        if(!args[0]){
            user.reply(461, {command: "PART"}); //ERR_NEEDMOREPARAMS
            return false;
        }
        
        var channel = this.server.getChannel(args[0]);
        
        if(!channel){
            user.reply(403, {'channel name': args[0]}); //ERR_NOSUCHCHANNEL
            return false;
        }
        
        if(!channel.getUser(user.id)){
            user.reply(442, {channel: channel.name}); //ERR_NOTONCHANNEL
            return false;
        }
        
        var msg = args[1];
        
        //Anuncia a saída e remove o usuário
        channel.write(user.response('self', 'BAN', msg, channel.name).message);
        channel.banUser(user);
        return true;
    }
};

module.exports = Commands;