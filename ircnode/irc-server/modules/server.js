﻿var net = require('net'),
    os  = require('os'), 
    User    = require('./user'), 
    Channel = require('./channel'), 
    ServerCommands = require('./comandosServidor');


function Server() {

    this.name      = "ad.si.inf.ufg";
    this.host      = os.hostname();
    this.address   = "localhost";
    this.version   = '0.3.0';
    this.port      = 6667;

    this.commands   = new ServerCommands(this);
    
    this.debugMode = false;
    this.opers     = require('../files/operators');
    this.admin = {
        name:  "Marcelo Akira Inuzuka", 
        email: "marceloakira@inf.ufg.br", 
        info:  "Mestre em Tecnologia da Informação e Comunicação na Formação em EaD"
    };

    this.users    = [];
    this.channels = [];
    
    this.connectionsCount = 0;
    this.opersOnline   = 0;
    this.commandCount  = [];
    this.receivedCount = 0;
    this.sentCount = 0;
    this.lastCommand = "";
    this.userid = 0; //Gerador de ID
    
}

Server.initialize = function () {
    var server = new Server();
    server.start();
    return server;
};

Server.prototype = {

    start: function () {

        var server = this;
        this.server = net.createServer(conn);
        this.server.listen(this.port);
        console.log('Servidor escutando em: ' + this.port);

        require('dns').lookup(require('os').hostname(), function (err, add, fam) {
          this.address = add;
        });

        function conn(client) {
            
            //Adiciona usuário à lista do servidor
            client.name = server.userid;
            
            user          = new User(client, server);
            user.type     = "node";
            user.id       = server.userid;
            user.nickname = "guest_"+user.id;
            user.host     = client.remoteAddress.replace(/([f:]+)/g, '');

            server.users[user.id] = user;
            server.connectionsCount++;
            server.userid++;
            
            client.on("data", function(text) {
                
                user = server.getUser(this.name);
                server.receivedCount++;
                
                var lines = text.toString().split(/\n/);
                
                for(var i=0; i<lines.length; i++){
                    if(lines[i])
                        server.send(user, lines[i]);
                }
            });

            client.on("error", function(err){
                //Elimina o usuário do servidor
                server.commands.QUIT(server.getUser(this.name), '');
            });
        }
    }, 

    setWebUser: function(socket, host, pass){
        
        var user = new User(socket, this);
        user.type     = "web";
        user.id       = this.userid;
        user.nickname = "guest_"+user.id;
        user.password = pass;
        user.host     = host;
        
        this.users[user.id] = user;
        this.connectionsCount++;
        this.userid++;

        user.countdown();
        
        return user;
    }, 

    destroy: function(msg){

        this.server.close();
        
        for (var i=0; i<this.users.length; i++) {
            if (this.users[i]) {
                this.users[i].response('server', 'SQUIT', msg, 'self').send();
                this.users[i].destroy();
                this.users[i] = null;
            }
        }
    }, 

    send: function (user, data) {

        var message = (typeof(data) == 'string')? this.parseMessage(data) : data;
        
        this.debug("Entrada: "+ JSON.stringify(message));

        if (this.findCommand(message.command, message.args)) {
            
            //Executa comando
            var status = this.commands[message.command](user, message.args);

            //Seta timestamp da última atividade do usuário
            user.set('access', new Date().getTime());

            //Se executou, incrementa contador
            if (status) {
                this.commandCounter(message.command);
            }
            //Se não retornou nenhuma mensagem, envia response em branco
            //Obs.: O cliente web envia um request
            //ele precisa receber uma response, mesmo que seja em branco
            if(user.socket){
                user.write();
            }
        }
        else {
            user.reply(421, {command: message.command}); //ERR_UNKNOWNCOMMAND
        }
    }, 

    parseMessage: function (data) {
        //Separa a mensagem
        var message = data.trim().split(/ :/),
        //Separa os argumentos
            args = message[0].split(' ');

        message = [message.shift(), message.join(' :')];

        //Se houver mensagem, insere na última posição do array dos argumentos
        if (message.length > 0) {
            args.push(message[1]);
        }

        if (data.match(/^:/)) {
            args[1] = args.splice(0, 1, args[1]);
            args[1] = (args[1] + '').replace(/^:/, '');
        }

        return {
            command: args[0].toUpperCase(),
            args: args.slice(1)
        };
    },

    commandCounter: function(command){
        if(this.commandCount[command]){
            this.commandCount[command]++;
        }
        else{
            this.commandCount[command] = 1;
        }
    }, 

    findCommand: function (command, args) {
        this.lastCommand = command + " " + args.toString().replace(",", " ").trim();
        return this.commands[command];
    }, 
    
    broadcast: function (message) {
        
        this.users.forEach(function(user) {
            user.write(message);
        });

        process.stdout.write("[Server] Broadcast: "+ message + "\r\n");
    }, 

    //Se a opção de debug estiver ativada, printa log no servidor
    debug: function(msg){

        if(typeof(msg) == 'object'){
            msg = JSON.stringify(msg);
        }

        if(this.debugMode)
            console.log("*** DEBUG: "+msg);
    }, 

    get: function(attr){
        this.debug("Server.get('"+attr+"')");
        return this[attr];
    }, 

    set: function(attr, value){
        this.debug("Server.set('"+attr+"', '"+value+"')");
        
        if(value === "++") this[attr]++;
        
        else if(value === "--") this[attr]--;
        
        else this[attr] = value;
    }, 

    //Recebe o ID ou Nickname de um usuário. Retorna um objeto da classe usuário ou "false".
    getUser: function(id, pass){
        
        this.debug("Server.getUser()");

        var response = false;

        //Password
        if ( (isNaN(id)) && (id.match(/[\.\:]/)) && (pass) ) {
            for(var i=0; i<this.users.length; i++){
                var user = this.users[i];
                if ((user) && (user.get('password') === pass) && (user.get('host') === id)) {
                    response = this.users[i];
                    break;
                }
            }
        }
        //Nickname
        else if(isNaN(id)){
            for(var i=0; i<this.users.length; i++){
                if ((this.users[i]) && (this.users[i].get('nickname') === id)) {
                    response = this.users[i];
                    break;
                }
            }
        }
        //ID
        else if(this.users[id]){
            response = (this.users[id])? this.users[id] : false;
        }

        return response;
    }, 

    //Recebe um nome de canal e o usuário criador. Insere um novo canal ao servidor.
    addChannel: function(name, op){
        
        this.debug("Server.addChannel()");

        if(this.getChannel(name) == false){ 
            this.channels.push(new Channel(this, name, op));
            return true;
        }

        else return false;
    }, 

    //Remove um canal
    removeChannel: function(id){

        this.debug("Server.removeChannel()");

        for (var i=0; i<this.channels.length; i++) {
            if (this.channels[i].name === id) {
                this.channels.splice(i, 1);
                break;
            }
        }
    }, 

    //Recebe o nome de um canal. Retorna um objeto da classe canal.
    getChannel: function(id){
        
        this.debug("Server.getChannel()");
        
        var response = false;
        
        for (var i=0; i<this.channels.length; i++) {
            if(this.channels[i].name === id){
                response = this.channels[i];
                break;
            }
        }

        return response;
    }
};

exports.Server = Server;

if (!module.parent) {
    Server.initialize();
}