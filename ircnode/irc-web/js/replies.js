/* */

function Replies(chat){
	
	this.chat = chat;

	this.execute = function(data){
		//Confere se existe tratamento para esse comando, se existir executa
		if (typeof this["R"+data.id] === 'function') this["R"+data.id](data);
	};
}

Replies.prototype.R001 = function(data){
	var nick = data.content.match(/([\w\-_]+)!(.*)@(.*)/)[1];
	chat.client.nick = nick;
};

//RPL_NAMES
Replies.prototype.R353 = function(data){
	
	var match = data.content.match(/(.*):(.*)/);
	var users = match[2].split(' ');
	var channel = match[1].match(/(.*)(([#&!+])(.*)) /)[2];

	if(talks.exists(channel)){
	
		for (i=0; i<users.length; i++) {
			if (users[i][0] == '@')
				talks.get(channel).addMember(users[i].slice(1, users[i].length), '@');
			else 
				talks.get(channel).addMember(users[i]);
		}
		
		if(talks.selected.id == channel)
			talks.topic();
	}
};

//RPL_TOPIC
Replies.prototype.R332 = function(data){

	var match = data.content.match(/(.*) :(.*)/);
	var channel = match[1];
	var topic = match[2];

	if (channel === talks.selected.id)
		$('#chat-topic').html(topic);
	
	talks.set(channel, 'status', topic);
	//talks.notice('Tópico do canal: '+topic, channel);
};