/* Objeto responsável pelos eventos da interface gráfica */

var events;

function Events(){

	//Seta todos os eventos dos objetos
	this.bind = function(){
		this.events.forEach(function(item){
			$(item.element).unbind(item.event);
			$(item.element).bind(item.event, function(e){ 
				item.action(e, this); 
			});
		});
	};

	//Eventos
	this.events = [
		
		{
			element: '.conversation', 
			event: 'click', 
			action: function(e, div){
				//Seleciona uma nova conversação
				talks.select($(div).attr('name'));
			}
		}, 

		{
			element: '.user .message-name', 
			event: 'click', 
			action: function(e, div){
				
				//Abre conversa privada com usuário
				var name = $(div).html();
				
				if ((name != chat.client.nick)&&(name != chat.server.name)) {
					if (talks.exists(name)) {
						talks.select(name);
					}
					else {
						var talk = new Talk(name, 'user');
						talks.add(talk, true);
					}
				}
			}
		}, 

		{
			element: '#message-text', 
			event: 'keydown', 
			action: function(e, div){

				//Se apertar Enter e o Shift não estiver apertado
				if ((e.keyCode == 13)&&(!e.shiftKey)) {

					var text = $(div).val();

					//Reseta o campo
					$(div).val('');
					e.preventDefault();

					//Se houver texto
					if(text){

						//Se for a aba do servidor, envia comando
						if(talks.selected.id === 'server'){
							var params = text.match(/^(\/|)([\w]+)($| (.*))/);
							
							if(params[4]){
								if( args = params[4].match(/([^:]+):(.*)/) ){

									args = args[1].split(" ").concat(args[2]);
									
									if(!args[args.length-2]){

										args[args.length-2] = args[args.length-1];
										args.splice(args.length-1, 1);
									}
								}
								else{
									args = params[4].split(" ");
								}
								chat.send(params[2], args);
							}
							else
								chat.send(params[2]);
						}

						//Senão envia mensagem de texto para conversa selecionada
						else{

							var notice = text.match(/^NOTICE:\s?(.*)/i);
							
							//Se for comando NOTICE
							if (notice) {
								chat.send("NOTICE", [talks.selected.id, notice[1]]);
							}
							//Se for comando PRIVMSG
							else {
								chat.send("PRIVMSG", [talks.selected.id, text]);
							}
						}

						//Adiciona a mensagem e printa na tela
						talks.message(text, talks.selected.id, chat.client.nick);
						chat.lastLine = text;
					}
				}
				//Seta pra cima
				else if(e.keyCode == 38){

					if(($(div).val() != chat.lastLine)&&(chat.lastLine)){

						chat.nextLine = ($(div).val())? $(div).val() : " ";
						$(div).focus().val('').val(chat.lastLine);
					}
					e.preventDefault();
				}
				//Seta pra baixo
				else if(e.keyCode == 40){

					if(chat.nextLine){

						$(div).focus().val('');
						if(chat.nextLine!=" ") $(div).val(chat.nextLine);
						chat.nextLine = '';
					}
					e.preventDefault();
				}
			}
		}, 

	];
}