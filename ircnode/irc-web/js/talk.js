/* Classe que representa uma conversa */

var emotes = [];

function Talk (name, type) {
	
	this.id       = '';
	this.prefix   = '';
	this.name     = '';
	this.type     = type;
	this.status   = '';
	this.color    = this.colorize(name);
	this.messages = [];
	this.members  = [];

	if (type == 'server') {
		this.id = 'server';
		this.name = name;
	}
	else if (type == 'channel') {
		this.id = name;
		this.prefix = name[0];
		this.name = name.slice(1, name.length);
	}
	else {
		this.id = name;
		this.name = name;
		this.members.push({name: name, type: ''});
	}
}	

Talk.prototype.get = function (attr) { 
	if(attr in this) return this[attr];
    else return false;
};

Talk.prototype.set = function (attr, value) { 

	if(attr in this) this[attr] = value;
};

Talk.prototype.add = function (content, sender, type, print) {

	//Monta objeto
	var msg = {
		content: content, 
		sender: sender, 
		origin: (sender == chat.client.nick)? 'me' : (sender == chat.server.name)? 'server' : 'user', 
		type: type, 
		time: this.timestamp()
	}

	//Adiciona ao array de mensagens
	this.messages.push(msg);

	//Limita a 500 mensagens salvas por conversa
	if(this.messages.length > 500){

	}

	//Adiciona à "última mensagem" da conversa
	var last = (msg.content.length > 30)? msg.content.slice(0, 30) + '...' : msg.content;
	$('.conversation[name="'+this.id+'"] .conversation-info').html(last);

	//Se não for a conversa atual, seta status de "nova mensagem"
	if (!print) $(".conversation[name='"+this.id+"'").addClass("new-messages");

	//Printa na tela
	if (print) {
		(msg.type == 'message')? this.printMessage(msg) : this.printNotice(msg);
		//Move a tela para a posição adequada
		$("#chat-container").stop().animate({scrollTop: $('#chat').height()}, 400);
	}
};

// Talk.addMember(member{});
// Talk.addMember(member.name);
// Talk.addMember(member.name, member.type);
Talk.prototype.addMember = function (member, type) {
	
	if (!type) type = '';

	if (typeof member === 'string') {
		if(!this.isMember(member))
			this.members.push({name: member, type: type});
	}
	else {
		if(!this.isMember(member.name))
			this.members.push(member);
	}
};

Talk.prototype.isMember = function (member) {
	
	for (var i=0; i<this.members.length; i++) {

		if (this.members[i].name == member) return true;
	}

	return false;
};

Talk.prototype.removeMember = function (member) {

	for (var i=0; i<this.members.length; i++) {

		if (this.members[i].name == member) this.members.splice(i, 1);
	}
};

Talk.prototype.getMember = function (member) {

	for (var i=0; i<this.members.length; i++) {

		if (this.members[i].name == member) return this.members[i];
	}

	return false;
};

// Talk.setMember(member.name, member{});
// Talk.setMember(member.name, attr, value);
Talk.prototype.setMember = function (id, member, value) {
	
	for (var i=0; i<this.members.length; i++) {

		if (this.members[i].name == id){
			if(value)
				this.members[i][member] = value;
			else
				this.members[i] = member;
		}
	}
};

Talk.prototype.printMessage = function (msg) {

	//Trata mensagem
	var message = msg.content;
	message = this.msgNormalize(message);
	message = this.msgHyperlink(message);
	message = (this.msgVideo(message) != message)? this.msgVideo(message) : this.msgImage(message);
	message = this.msgEmote(message);
	message = this.msgNotice(message);

	//Se já existir um balão atual, insere dentro dele
	if (($('#chat .message:last .message-time').html() == msg.time)&&($('#chat .message:last .message-name').html() == msg.sender)) {
		$("<div class='message-content'>"+message+"</div>").insertAfter('#chat .message:last .message-content:last');
	}
	//Senão cria um novo balão
	else {
		//Monta a mensagem
		$('#chat').append("<div class='message-container'><div class='message "+msg.origin+"'>"+
						  "<div class='message-time'>"+msg.time+"</div>"+
						  "<div class='message-name n"+this.colorize(msg.sender)+"'>"+msg.sender+"</div>"+
						  "<div class='message-content'>"+message+"</div>"+
						  //"<div class='message-time'>"+msg.time+"</div>"+
						  "</div></div>");
	}

	//Seta eventos
	events.bind();
};

Talk.prototype.printNotice = function (msg) {

	//Adiciona notícia
	$('#chat').append("<div class='notice'>"+
					  "<div class='message-content'>"+msg.content+"</div>"+
					  "<div class='message-time'>"+msg.time+"</div>"+
					  "</div>");
};

Talk.prototype.print = function (from) {

	//Número de mensagens que irão ser printadas
	var lines = 30;

	//Printa toda a conversação
	if(!from){
		$('#chat').html('');
		from = (this.messages.length<lines)? 0 : this.messages.length - lines;
	}
	
	for (var i=from; i<this.messages.length; i++) {
		msg = this.messages[i];
		(msg.type == 'message')? this.printMessage(msg) : this.printNotice(msg);
	}
	
	//Move a tela para a posição adequada
	$("#chat-container").scrollTop($('#chat').height());
};

Talk.prototype.colorize = function (name) {
	
	var code = 0;
	
	for (var i=0; i<name.length; i++) {
		code += name.charCodeAt(i);
	}

	code %= 5;
	
	return code+1;
};

Talk.prototype.timestamp = function () {
	
	var date   = new Date();
	var hour   = date.getHours();
	var minute = date.getMinutes();

	hour   = (hour<10)?   '0'+hour : hour;
	minute = (minute<10)? '0'+minute : minute;

	return hour+":"+minute;
};

Talk.prototype.msgNormalize = function (message) {
	//Remove caracteres proibídos
	return message.replace(/</g, '&lt;').replace(/>/g, '&gt;');
};

Talk.prototype.msgHyperlink = function (message) {
	return message.replace(
		/(^|\s)([\w]+\:\/\/|www\.)([^ ]+)/g, 
		"$1<a href='$2$3' target='_blank'>$2$3</a>"
	);
};

Talk.prototype.msgVideo = function (message) {
	//Incorpora vídeo do youtube
	var match = message.match(/(http(?:s?):\/\/(?:www\.)?youtu(?:be\.com\/watch\?v=|\.be\/)([\w\-\_]*)(&(amp;)?‌​[\w\?‌​=]*)?)/);
	
	if (match) { 
		message += '<iframe class="youtube" src="https://www.youtube.com/embed/'+ match[2] +'" allowfullscreen></iframe>';
	}
	
	return message;
};

Talk.prototype.msgImage = function (message) {

	//Incorpora imagem
	append = message.match(/([a-z\-_0-9\/\:\.]*\.(jpg|jpeg|png|gif))/i);

	if (append) {

		message += '<a href="'+append[1]+'" target="_blank"><img class="image" style="display:none" src="'+append[1]+'"/></a>';

		imageExists(append[1], function (exists, url) {
			if (!exists) $('#chat .message img[src="'+url+'"]').remove();
			else { 
				$('#chat .message img[src="'+url+'"]').show();
				$("#chat-container").scrollTop($('#chat').height());
			}
		});
	}

	function imageExists(url, callback) {
	  var img = new Image();
	  img.onload = function () { callback(true, url); };
	  img.onerror = function () { callback(false, url); };
	  img.src = url;
	}

	return message;
};

Talk.prototype.msgEmote = function (message) {

	var emote;
	var file;

	for(var i=0; i<emotes.length; i++){
		
		emote = emotes[i][0];
		file  = emotes[i][1];

		emote = this.msgNormalize(emote).replace(/([\W])/g, "\\$1");
		
		regex = new RegExp("(^| )("+emote+")( |$)", 'g');

		message = message.replace(regex, "$1<img class='emote' src='img/emotes/"+file+".png'>$3");
	}

	return message;
};

Talk.prototype.msgNotice = function (message) {

	var match = message.match(/^NOTICE:\s?(.*)/i);
	return (match)? '<b>'+match[1]+'</b>' : message;
};