/* WEB IRC */

$(document).ready(function(){
	
	//Seta classes de processamento de dados
	chat.commands = new Commands(chat);
	chat.replies  = new Replies(chat);

	//Inicia comunicação com servidor
	chat.start(250);
	
	//Seta eventos do chat
	events = new Events();
	events.bind();

	//Cria janela de conversação com o servidor
	var server = new Talk('Servidor IRC', 'server');
	server.set('status', chat.server.stat);
	talks.add(server, true);

	//Importa emotes
	$.getJSON('json/emotes.json').done(function(data){
		emotes = data;
	});

});


var chat = {

	client: {
		pass: $.md5(new Date()), 
		user: "web_client", 
		host: "web.irc.inf.ufg.br", 
		nick: "WIRED", 
		name: "guest", 
		chan: "#ad-si-2016-2"
	}, 
	server: {
		host: "localhost", 
		port: 8081, 
		rest: "rest", 
		name: "Servidor IRC", 
		stat: "Envie comandos do protocolo IRC"
	}, 
	interval: null, 
	errors: 0, 
	lastLine: '', 
	nextLine: '', 
	sendList: []
};

chat.start = function(time){

	//Registra conexão com o servidor IRC
	chat.connect();

	//Seta o intervalo de acesso ao servidor
	chat.interval = setInterval(function(){
		chat.send();
	}, time);
};

//Registro de conexão IRC	
chat.connect = function(){

	//PASS: Obrigatório para o cliente Web ser identificado
	chat.send("PASS", chat.client.pass);

	//NICK: Opcional, caso não haja Nickname, recebe nome aleatório
	if(chat.client.nick)
		chat.send("NICK", chat.client.nick);

	//USER: Completa o registro de conexão
	chat.send("USER", [chat.client.user, "0", "*", chat.client.name]);
	
	//JOIN: Caso haja canal preferencial, se conecta automaticamente
	if(chat.client.chan)
		chat.send("JOIN", chat.client.chan);
};

chat.send = function(command, args){

	//Trata comando
	command = (typeof command != 'undefined')? command.toUpperCase() + "/" : "";
	
	//Sem parâmetros
	if (!args) {
		var params = "";
	}
	//Um parâmetro (string)
	else if (typeof(args)==='string') {
		var params = encodeURIComponent(args) + "/";
	}
	//Mais de um parâmetro (array)
	else {
		var params = "";
		args.forEach(function(arg){
			params += encodeURIComponent(arg) + "/";
		});
	}


	//Adiciona na fila
	if ((command)&&(chat.sendList.length)) {
		this.sendList.push({command: command, params: params});
	}
	//Envia comando
	else if ( (!chat.sendList.length) || ((!command)&&(chat.sendList.length)&&(chat.sendList[0])) ) {

		//Extrai as informações da fila
		if ((!command)&&(chat.sendList.length)) {
			command = chat.sendList[0].command;
			params  = chat.sendList[0].params;
			chat.sendList[0] = false;
		}
		//Seta "comando em execução"
		else {
			chat.sendList.push(false);
		}

		var url = "http://"+chat.server.host+":"+chat.server.port+"/"+chat.server.rest+"/"+chat.client.pass+"/"+command+params;

		$.ajax({
			type: "GET", 
			dataType: "json", 
			url: url
		}).done(function(data){ 
			
			//Comunicação bem sucedida, envia para tratamento
			chat.data(data);
			
			//Remove da fila
			chat.sendList.splice(0,1);
			
			//Se tiver algum na fila, executa
			if(chat.sendList.lenght){
				chat.send();
			}

		}).fail(function(e){
			//Erro de comunicação
			chat.error(e);
		});
	}
};

chat.data = function(responses){

	errors = 0;

	responses.forEach(function(response){
		if(response.sender && response.id){

			console.log(response);

			//Seta o tipo da origem (usuário ou servidor)
			response.origin = (response.sender.match(/^[\w-_]+!(.*)@(.*)/))? 'user' : 'server';
			
			if(response.origin == 'user'){
				var match = response.sender.match(/^(.*)!(.*)@(.*)$/);
				response.nick = match[1];
				response.user = match[2];
				response.host = match[3];
				if(response.nick == chat.client.nick) response.origin = 'me';
			}
			response.id = response.id.toUpperCase();
			//response.content = response.content.replace(/^:/, '');

			//Printa response no servidor
			if ((response.origin == 'server')&&(response.id != "PONG")) 
				talks.message('['+response.sender+'] '+response.id+" "+response.content, 'server', chat.server.name);
			
			//Reply
			if(response.id.match(/\d\d\d/)) chat.replies.execute(response);
			
			//Command
			else chat.commands.execute(response);
		}
	});
};

chat.error = function(e){

	chat.errors++;

	if(chat.errors >= 3){
		console.log("Conexão com o servidor interrompida");
		clearInterval(chat.interval);
	}

};

chat.popup = function(){

};

chat.get = function(attr){
	if(attr in chat) return chat[attr];
    else return false;
};

chat.set = function(attr, value){
	if(attr in chat) chat[attr] = value;
};
