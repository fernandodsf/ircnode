/* */

function Commands() {
	
	this.execute = function (data) {
		//Confere se existe tratamento para esse comando, se existir executa
		if (typeof this[data.id] === 'function') this[data.id](data);
	};
}

Commands.prototype.JOIN = function (data) {

	if (data.nick == chat.client.nick) {
		var talk = new Talk(data.target, 'channel');
		talks.add(talk, true);
	}
	talks.notice('<b>'+data.nick+'</b> entrou no canal', data.target, data.nick);

};

Commands.prototype.PART = function (data) {

	if(data.nick == chat.client.nick){
		talks.remove(data.target);
	}
	else{
		talks.get(data.target).removeMember(data.nick);
		talks.notice("<b>"+data.nick+"</b> saiu do canal", data.target, data.nick);
	}

};

Commands.prototype.QUIT = function (data) {

	if(data.nick == chat.client.nick){
		talks.remove(data.target);
	}
	else{
		talks.get(data.target).removeMember(data.nick);
		talks.notice("<b>"+data.nick+"</b> se desconectou", data.target, data.nick);
	}

};

Commands.prototype.NICK = function (data) {

	if (data.nick === chat.client.nick) {

		chat.client.nick = data.content;
		talks.notice('<b>'+data.nick+'</b> mudou seu nome para <b>'+data.content+'</b>', null, data.nick);
	}

	else {

		var notice = '<b>'+data.nick+'</b> mudou seu nome para <b>'+data.content+'</b>';

		if (talks.exists(data.nick)) {
			
			var messages = talks.get(data.nick, 'messages');
			var select = talks.isSelected(data.nick);
			
			var talk = new Talk(data.content, 'user');
			talk.set('messages', messages);
			
			talks.add(talk, select);
			talks.remove(data.nick);

			talks.notice(notice, data.content, data.nick);
		}

		var channels = talks.member(data.nick);

		for (var i=0; i<channels.length; i++) {

			key = channels[i];
			
			if(key != data.content){

				talks.notice(notice, key, data.nick);
				talks.get(key).setMember(data.nick, 'name', data.content);
			}
		}
	}

};

Commands.prototype.PRIVMSG = function (data) {
	//Se não for uma mensagem enviada pelo cliente
	if (data.nick != chat.client.nick) {
		//Se existir a conversa (canal)
		if (talks.exists(data.target)) {
			talks.message(data.content, data.target, data.nick);
		}
		//Mensagem privada
		else if (data.target == chat.client.nick) {
			//Se a conversa existir
			if (talks.exists(data.nick)) {
				talks.message(data.content, data.nick, data.nick);
			}
			//Senão
			else {
				var talk = new Talk(data.nick, 'user');
				talks.add(talk);
				talks.message(data.content, data.nick, data.nick);
			}
		}
	}
};

Commands.prototype.NOTICE = function (data) {
	//Se não for uma mensagem enviada pelo cliente
	if (data.nick != chat.client.nick) {
		//Se existir a conversa (canal)
		if (talks.exists(data.target)) {
			talks.message('Notice:'+data.content, data.target, data.nick);
		}
		//Mensagem privada
		else if (data.target == chat.client.nick) {
			//Se a conversa existir
			if (talks.exists(data.nick)) {
				talks.message('Notice:'+data.content, data.nick, data.nick);
			}
			//Senão
			else {
				var talk = new Talk(data.nick, 'user');
				talks.add(talk);
				talks.message('Notice:'+data.content, data.nick, data.nick);
			}
		}
	}
};