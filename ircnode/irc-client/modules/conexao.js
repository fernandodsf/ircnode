var net = require('net');

exports.connect = function (server, params) {

    var socket = new net.Socket();
    var msg = null;

    socket.connect(server._port, server._address, function () {
    
        //PASS
        var date = new Date();
        socket.write("PASS "+ date.getUTCHours()+":"+date.getUTCMinutes() +"\r\n");
        //NICK
    	if(params.nickname) socket.write("NICK "+ params.nickname +"\r\n");
        //USER
    	socket.write("USER "+ params.username +" 0 * :"+ params.realname +"\r\n");
        //JOIN
    	socket.write("JOIN "+ params.channel + "\r\n");

        server._socket = socket;
    });

    socket.on('error', function (err) {
        if(server.debug)
            server.print("Erro: "+err);
        //process.exit();
    });

    socket.on('data', function (data) {
        if (data.slice(0, 4) == "PING") {
            server.print('Received: ' + data);
            var reply = "PONG " + data.slice(5);
            server.print("replying with " + reply);
            socket.write(reply);
        } 
        else {
            
            var lines = data.toString('utf8').split(/\n/g);
            
            if(!lines[lines.length-1]) lines.splice(lines.length-1, 1);
            
            lines.forEach(function(msg){
                server.data(msg);
            });    
        }
    });

    socket.on('close', function () {
    	process.stdout.clearLine();
		process.stdout.cursorTo(0);
        server.print("Disconnected from server: Connection to IRC server lost.");
    });
};

var Future = function(time){
	this.time = (time)? time : 300;
	this.actual = 0;
};

Future.prototype = {
	exec: function(fn){
		fn();
		return this;
	}, 
	then: function(fn){
		this.actual += this.time;
		setTimeout(function(){
			fn();
		}, this.actual);
		return this;
	}
};