# IRC NODE.js

Repositório do primeiro projeto da disciplina de Aplicações distribuídas do curso de Sistemas de Informação da UFG
--
Para rodar o cliente base, instale o node.js em seu computador. Feito isso basta configurar os dados no arquivo cliente.js.

Para obter o ircnode:
```
git clone https://gitlab.com/ad-si-2016-2/ircnode.git
```

Entre na pasta do projeto:
```
cd ircnode
```

Para rodar o servidor:
```
cd irc-server
```

Para conectar:
```
cd irc-client
node cliente.js
```

Para entrar em um canal:
```
/join #nome_do_canal
```

Depois de ter acessado um canal, para mandar uma mensagem:
```
/msg #nome_do_canal sua mensagem
```
