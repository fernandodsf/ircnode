﻿MEMBROS DO PROJETO
==================

* Professor: Marcelo Akira Inuzuka - @marceloakira

GRUPO 1
-------
* Líder: CAIO LENTHULLIUS BARROS
* ALLAN DA SILVA BRAGA 100349
* JOSE UMBERTO MOREIRA JUNIOR
* POLLYANNA TELES PINCHEMEL
* 
* 

GRUPO 2
-------
* Líder: Edvan Santos Souza
* Sílvio Passos Severino
* Gabriel Hamada Santalucia
* Tiago Moreira vaz
* Felipe Neves Rocha da Silva

GRUPO 3
-------
* Líder: Gilmar Alves Bernardes Júnior
* Fernando dos Santos Figueredo
* Raquel Leite Andrade
* Walmir Nascimento Batista de Melo 
* Ilda Emanoely Ribeiro Silva Neta
* Jeniffer Silva Pires de Moraes

GRUPO 4
-------
* Líder: Pedro de Oliveira
* Ygor Escatolim Castor Santana
* Carlos Hernane de Oliveira
* Rafhael Augusto Rezende Ferreira
* Gustavo Alves de Souza
* Gleydson Eugênio

GRUPO 5
-------
* Líder: Vitor Roberto Silva Miranda
* ADRIEL FERREIRA ROSA
* Guilherme Souza Santos 
* Michel Ferreira da Silva
* Rodrigo Araújo Leonardo
* Julio Cesar Gonzales A.
